FROM adoptopenjdk/openjdk15:alpine-jre

ADD target/api-gateway-*-SNAPSHOT.jar /api-gateway.jar

ENV JAVA_OPTS = $JAVA_OPTS

CMD ["sh", "-c", "java ${JAVA_OPTS} -jar /api-gateway.jar"]

EXPOSE 8080