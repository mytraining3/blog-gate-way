package com.example.apigateway.config;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.security.oauth2.resource.ResourceServerTokenServicesConfiguration;
import org.springframework.boot.autoconfigure.security.oauth2.resource.UserInfoTokenServices;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.ReactiveAuthenticationManagerAdapter;
import org.springframework.security.config.annotation.web.reactive.EnableWebFluxSecurity;
import org.springframework.security.config.web.server.ServerHttpSecurity;
import org.springframework.security.oauth2.client.OAuth2ClientContext;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.client.resource.OAuth2ProtectedResourceDetails;
import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationManager;
import org.springframework.security.web.server.SecurityWebFilterChain;
import org.springframework.web.util.DefaultUriBuilderFactory;

import static org.springframework.http.HttpMethod.POST;

@EnableWebFluxSecurity
@RequiredArgsConstructor
@Import(ResourceServerTokenServicesConfiguration.class)
public class SecurityConfig {

    private static final String[] SWAGGER_ENDPOINTS = {"/swagger-ui.html*", "/webjars/**", "/api/**", "/v2/**", "/swagger-resources/**"};
    private static final String[] PERMITTED_ENDPOINTS = {"/oauth/token", "/users"};
    private static final String ACTUATOR_ENDPOINT = "/actuator/**";

    private static final String RESOURCE_ID = "oauth2-resource";

    private final UserInfoTokenServices userInfoTokenServices;

    @Bean
    public SecurityWebFilterChain configure(ServerHttpSecurity http) {
        http.authorizeExchange()
                .pathMatchers(POST, SWAGGER_ENDPOINTS).permitAll()
                .pathMatchers(PERMITTED_ENDPOINTS).permitAll()
                .pathMatchers(ACTUATOR_ENDPOINT).permitAll()
                .anyExchange().authenticated()
                .and().csrf().disable()
                .httpBasic().disable()
                .oauth2ResourceServer().jwt()
                .authenticationManager(new ReactiveAuthenticationManagerAdapter(oauthAuthenticationManager()));
        return http.build();
    }

    private AuthenticationManager oauthAuthenticationManager() {
        OAuth2AuthenticationManager oauthAuthenticationManager = new OAuth2AuthenticationManager();
        oauthAuthenticationManager.setResourceId(RESOURCE_ID);
        oauthAuthenticationManager.setTokenServices(userInfoTokenServices);
        return oauthAuthenticationManager;
    }

    @Bean
    @Qualifier("userServiceTemplate")
    public OAuth2RestTemplate restTemplate(OAuth2ProtectedResourceDetails resource,
                                           @Qualifier("oauth2ClientContext") OAuth2ClientContext context,
                                           @Value("${blog.userservice.url}") String serviceUrl) {
        OAuth2RestTemplate oAuth2RestTemplate = new OAuth2RestTemplate(resource, context);
        oAuth2RestTemplate.setUriTemplateHandler(new DefaultUriBuilderFactory(serviceUrl));
        return oAuth2RestTemplate;
    }
}
