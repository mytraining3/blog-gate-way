package com.example.apigateway.entity;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Date;

@Data
@AllArgsConstructor
public class LastSeenUpdateRequest {
    private Date lastSeen;
}
