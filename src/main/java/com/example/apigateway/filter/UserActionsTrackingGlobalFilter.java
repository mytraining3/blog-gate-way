package com.example.apigateway.filter;

import com.example.apigateway.service.UserUpdateService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

@Slf4j
@Component
@RequiredArgsConstructor
public class UserActionsTrackingGlobalFilter implements GlobalFilter {

    private final UserUpdateService userUpdateService;

    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        log.info("UserActionsTrackingGlobalFilter start executing");
        return chain
                .filter(exchange)
                .then(exchange.getPrincipal().doOnNext(userUpdateService::updateLastSeenTime).then());
    }
}
