package com.example.apigateway.service;

import java.security.Principal;

public interface UserUpdateService {

    void updateLastSeenTime(Principal principal);
}
