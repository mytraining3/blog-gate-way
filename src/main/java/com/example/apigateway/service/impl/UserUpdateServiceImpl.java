package com.example.apigateway.service.impl;

import com.example.apigateway.entity.LastSeenUpdateRequest;
import com.example.apigateway.service.UserUpdateService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.security.Principal;
import java.util.Map;

import static java.sql.Timestamp.valueOf;
import static java.time.LocalDateTime.now;
import static java.util.Collections.singletonList;
import static org.springframework.http.MediaType.APPLICATION_JSON;

@Service
@Slf4j
@RequiredArgsConstructor
public class UserUpdateServiceImpl implements UserUpdateService {

    private static final String UPDATE_USER_LAST_SEEN_ENDPOINT = "/users/{id}/lastSeen";

    @Qualifier("userServiceTemplate")
    private final RestTemplate restTemplate;

    @Override
    public void updateLastSeenTime(Principal principal) {
        log.info("Start updating Last Seen Time");
        OAuth2Authentication authentication = (OAuth2Authentication) principal;
        Map<String, Object> details = (Map<String, Object>) authentication.getUserAuthentication().getDetails();
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(singletonList(APPLICATION_JSON));
        HttpEntity<LastSeenUpdateRequest> entity = new HttpEntity<>(new LastSeenUpdateRequest(valueOf(now())), headers);
        restTemplate.put(UPDATE_USER_LAST_SEEN_ENDPOINT, entity, details.get("id"));
    }
}